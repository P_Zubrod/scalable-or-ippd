# -*- coding: utf-8 -*-

import re
from pyspark.sql import *
from pyspark.sql.functions import *
from manager import MethodsManager


#Paul Zubrod
@MethodsManager.register("core/column-rename")
def core_column_rename(cmd, df,**kwargs):
    return df.withColumnRenamed(cmd["oldColumnName"], cmd["newColumnName"])


#Paul Zubrod
@MethodsManager.register("core/column-removal")
def core_column_removal(cmd, df,**kwargs):
    return df.drop(cmd["columnName"])


'''
@MethodsManager.register("core/column-move")
def core_column_move(cmd, df):
    columns = df.columns[:]
    current_index = columns.index(cmd["columnName"])
    columns.insert(cmd["index"], columns.pop(current_index))

    replace_order = [i for i in range(len(columns))]
    replace_order.insert(cmd["index"], replace_order.pop(current_index))

    rdd_moved = df.map(lambda row: [row[i] for i in replace_order])
    return df.sql_ctx.createDataFrame(rdd_moved, columns)
'''

#Paul Zubrod
@MethodsManager.register("core/column-move")
def core_column_move(cmd,df,**kwargs):
    col = cmd["columnName"]
    pos = cmd["index"]

    currpos = df.columns.index(col)
    nextcol = df.columns[pos]

    oldcolnewname = col + "1"
    colsavenewname = nextcol + "1"

    df = df.withColumn(oldcolnewname,
df[col]).withColumn(colsavenewname,df[nextcol])
    swap = df.withColumn(nextcol,
df[oldcolnewname]).withColumn(col,df[colsavenewname]).drop(oldcolnewname).drop(colsavenewname).withColumnRenamed(col,colsavenewname).withColumnRenamed(nextcol,col).withColumnRenamed(colsavenewname,nextcol)

    if(pos < currpos):
        return move_column(swap,swap.columns[currpos],pos+1)
    elif(pos > currpos):
        return move_column(swap,swap.columns[currpos],pos-1)
    else:
        return swap


#Paul Zubrod
def move_column(data,col,pos):
    currpos = data.columns.index(col)
    nextcol = data.columns[pos]

    oldcolnewname = col + "1"
    colsavenewname = nextcol + "1"

    data = data.withColumn(oldcolnewname,
data[col]).withColumn(colsavenewname,data[nextcol])
    swap = data.withColumn(nextcol,
data[oldcolnewname]).withColumn(col,data[colsavenewname]).drop(oldcolnewname).drop(colsavenewname).withColumnRenamed(col,colsavenewname).withColumnRenamed(nextcol,col).withColumnRenamed(colsavenewname,nextcol)

    if(pos < currpos):
        return move_column(swap,swap.columns[currpos],pos+1)
    elif(pos > currpos):
        return move_column(swap,swap.columns[currpos],pos-1)
    else:
        return swap


@MethodsManager.register("core/row-removal")
def core_row_removal(cmd, df=None,**kwargs):
    funcs = []
    for facet in cmd["engineConfig"]["facets"]:
        pos = df.columns.index(facet["columnName"])
        query = facet["query"]
        query_lower = query.lower()
        if facet["mode"] == "text":
            if facet["caseSensitive"] is False:
                funcs.append(lambda e: query_lower not in e[pos].lower())
            else:
                funcs.append(lambda e: query not in e[pos])
        else:
            if facet["caseSensitive"] is False:
                funcs.append(lambda e: re.search(query, e[pos], re.IGNORECASE) is None)
            else:
                funcs.append(lambda e: re.search(query, e[pos]) is None)

    result = df.rdd.filter(lambda row: False not in [f(row) for f in funcs])
    return df.sql_ctx.createDataFrame(result, df.columns)


@MethodsManager.register("core/column-split")
def core_column_split(cmd, df=None,**kwargs):
    column_names = df.columns[:]
    pos = column_names.index(cmd["columnName"])

    before_columns = column_names[:pos]
    after_columns = column_names[pos + 1:]

    if "fieldLengths" in cmd:
        raise NotImplementedError("'fieldLengths' doesn't implemented")
    elif cmd.get("regex") is True:
        # TODO: cmd["maxColumns"]
        func = lambda e: e[:pos + 1] + tuple(re.split(cmd["separator"], e[pos])) + e[pos + 1:]
    else:  # TODO: cmd["maxColumns"]
        func = lambda e: e[:pos + 1] + tuple(e[pos].split(cmd["separator"])) + e[pos + 1:]

    result = df.sql_ctx.createDataFrame(df.map(func))

    new_column_names = (
        before_columns +
        [cmd["columnName"]] +
        ["%s %s" % (
            cmd["columnName"], i + 1) for i in range(len(result.columns) - len(df.columns))] +
        after_columns)

    for index, name in enumerate(new_column_names):
        result = result.withColumnRenamed("_%d" % (index + 1), name)

    if cmd.get("removeOriginalColumn") is True:
        result = result.drop(cmd["columnName"])
    return result


#Paul Zubrod
@MethodsManager.register("core/text-transform")
def core_text_transform(cmd, df,**kwargs):
    op = cmd["expression"]
    col = cmd["columnName"]
    if (op == "value.toUppercase()"):
        return df.withColumn(col, upper(df[col]))
    elif (op == "value.toLowercase()"):
        return df.withColumn(col, lower(df[col]))
    elif (op == "value.toTitlecase()"):
        title = udf(lambda s: s.title())
        return df.withColumn(col, title(df[col]))
    elif (op == "null"):
        blank_out = udf(lambda s : None)
        return df.withColumn(col,blank_out(df[col]))
    ### Georg Weisert ###
    #   the remaining column transforms are interpretations of cell data types
    #   since DataFrames have a single type per column these don't apply here
    #   a cell's data type interpretation is mostly relevant to facets
    #   where it will be emulated as needed
    ######################
    else:
        return df


#Paul Zubrod
@MethodsManager.register("core/fill-down")
def core_fill_down(cmd, df,**kwargs):
    col = cmd["columnName"]
    dff = df.select(col).collect()
    for key, value in kwargs.items():
        sql_ctx = value
    index = -1
    for i in dff:
        index = index + 1
        if i[col] != "":
            pass
        else:
            try:
                dff[index] = dff[index-1]
            except:
                print "error"
                continue

    return sql_ctx.createDataFrame(dff)


#Paul Zubrod
@MethodsManager.register("core/multivalued-cell-split")
def core_multivalued_cell_split(cmd,df,**kwargs):
    return df.withColumn(cmd["columnName"],split(df[cmd["columnName"]],cmd["separator"]))


