# -*- coding: utf-8 -*-
##################
#author: Paul Zubrod
#name: Scalable Openrefine Interpreter
##description: Create a data sample or use whole data to apply your openrefine programm
#################################

def usage(msg):
    print """
    ARGS
        [CSV] path to CSV
        [CMD] path to OpenRefine Programm
        [Spark_Home]* optional path to Spark folder


    """
    exit(msg)

from pyspark import SparkContext,SparkConf
from pyspark.sql.readwriter import DataFrameWriter
from pyspark.sql import SQLContext
from pyspark.sql import *
import sys,os,csv,json
from manager import *
from methods import *

#set environmentvariable if wanted
if len(sys.argv) == 4:
    from config import set_environment_variable
    set_environment_variable(sys.argv[3])


#Create SparkContext
try:
    sc = SparkContext("local", "ScalableOR")
    sc.setLogLevel("FATAL")

    sqlContext = SQLContext(sc)
except:
    usage("Environment Variables aren't set!")


#import data/log/sample
def import_data_toDF(f):
    df = sqlContext.read.load(f,format='com.databricks.spark.csv',header='true',inferSchema='true')
    return df


def export_data_to_csv(df_exp, filename):
    df_exp.write.format('com.databricks.spark.csv').options(delimiter="\t", codec="org.apache.hadoop.io.compress.GzipCodec").save('mycsv.csv')

def write_to_csv(df, filename):
    file = open(filename, "w")

    for col in df.columns:
        if ',' in col:
            file.write('"' + col + '"')
        else:
            file.write(col)
        file.write(',')

    file.seek(file.tell() - 1)
    file.write('\n')

    for row in df.rdd.collect():
        for element in row:
            if element is None:
                pass
            elif type(element) == str and "," in element:
                file.write('"' + element + '"')
            else:
                file.write(str(element))
            file.write(',')
        file.seek(file.tell() - 1)
        file.write('\n')

    file.close()


def import_data_sample(f,fraction):
    df = sqlContext.read.load(f,format='com.databricks.spark.csv',header='true',inferSchema='true')
    return df.sample(False,fraction)


def load_OR_programm(json_file_path):
    with open(json_file_path) as json_file:
        json_data = json.load(json_file)
    return json_data
    return json_data


def sample_data(file_path, n):
    outfileno = 1
    outfile = None
    filename = os.path.splitext(os.path.basename(file_path))[0]
    outfilename = filename +"_sample.csv".format(outfileno)
    try:
        outfile = open(outfilename, 'w')
        print "sample data successful created"
        print "Filename:  " + outfilename
    except:
        exit("Error while creating sample file")

    with open(file_path, 'r') as infile:
        writer = csv.writer(outfile)
        for index, row in enumerate(csv.reader(infile)):
            if index == n:
                break
            writer.writerow(row)
    df = import_data_toDF(outfilename)
    return df


def sort_column(data,col,order="asc"):
    if(order == "asc"):
        return data.sort(data[col].asc())
    elif(order == "desc"):
        return data.sort(data[col].asc())



if __name__ == "__main__":
    if len(sys.argv) < 3:
        usage("Arguments are missing")

    sample = raw_input("sample: number of rows: (type 'all' for whole file) \n")
    try:
        sample = int(sample)
    except:
        pass
    if type(sample) is type(1):
        if os.path.isabs(sys.argv[1]):
            dataframe = sample_data(sys.argv[1],sample)
            jfp = sys.argv[2]
        else:
            dataframe = sample_data(os.getcwd() + "/" + sys.argv[1], sample)
            jfp = os.getcwd() + "/" + sys.argv[2]
    else:
        print "use whole file"
        if os.path.isabs(sys.argv[1]):
            dataframe = import_data_toDF(sys.argv[1])
            jfp = sys.argv[2]
        else:
            dataframe = import_data_toDF(os.getcwd() + "/" + sys.argv[1])
            jfp = os.getcwd() + "/" + sys.argv[2]

    size = dataframe.count()
    dataframe.show()
    print dataframe.count()

    #openrefine programm
    commands = load_OR_programm(jfp)
    for cmd in commands:
        dataframe = MethodsManager.call(cmd, dataframe,sql_ctx = sqlContext)

    dataframe.show()
    dataframe.printSchema()
    print dataframe.count()


    #further options
    prompt = ""
    while prompt != "y":
        prompt = raw_input("sort? (y/n) \n")
        if prompt == "n":
            break
        if prompt == "y":
            column = raw_input("Column you want sort by \n")
            try:
                dataframe = sort_column(dataframe,column)
                dataframe.show()
            except:
                print "Column doesn't exist"

    prompt = ""
    while prompt != "y":
        prompt = raw_input("Export? (y/n) \n")
        if prompt == "n":
            break
        if prompt == "y":
            filename = raw_input("filename \n")
            export_data_to_csv(dataframe,filename)

    dataframe.show()


