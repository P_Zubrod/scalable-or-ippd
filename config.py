###################
#author: Paul Zubrod
#description:
#configures your Spark installation for Scalable OR Programm
########################

import os, sys, shutil


def integrate_spark_csv(spark_home):
    dst = spark_home + "/spark-csv"
    try:
        shutil.copytree("./spark-config/spark-csv/", dst)
    except:
        print "Folder already exists"

def spark_config(spark_home):
    with open(spark_home + "/conf/spark-defaults.conf", "a") as f:
        f.write("\nspark.driver.extraClassPath " + spark_home.rstrip("/") + "/spark-csv/*")

def set_environment_variable(spark_home):
    os.environ["SPARK_HOME"] = spark_home
    os.environ["PYTHONPATH"] = spark_home + "/python"
    print "SPARK_HOME = " + os.environ["SPARK_HOME"]
    print "PYTHONPATH = " + os.environ["PYTHONPATH"]
    print "Environmentvariables are set!"


if __name__ == "__main__":
    try:
        print os.environ["SPARK_HOME"]
        spark_home = os.environ["SPARK_HOME"]
    except KeyError:
        if len(sys.argv) < 2:
            exit("1 argument: Path to your Spark folder")
        else:
            spark_home = sys.argv[1]

    integrate_spark_csv(spark_home)
    spark_config(spark_home)


