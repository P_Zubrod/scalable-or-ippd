# -*- coding: utf-8 -*-


class MethodsManager(object):
    fn = {}

    @staticmethod
    def register(name):
        def register_(func):
            MethodsManager.add(name, func)
            return func
        return register_

    @staticmethod
    def add(name, func):
        MethodsManager.fn[name] = func

    @staticmethod
    def has(name):
        return name in MethodsManager.fn

    @staticmethod
    def call(cmd, df,**kwargs):
        name = cmd["op"]
        if MethodsManager.has(name):
            # v1.0
            return MethodsManager.get(name)(cmd, df,**kwargs)
        else:
            raise NotImplementedError("Method '%s' doesn't found" % name)

    @staticmethod
    def get(name):
        return MethodsManager.fn[name]
