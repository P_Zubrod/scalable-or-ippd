# README #

# Setup #

```
Install Spark from "http://spark.apache.org/downloads.html"
Set environment variables
SPARK_HOME = path to spark folder
PYTHONPATH = path to spark python folder

Integrate spark-config folder into your spark folder
```
# USAGE #
```
run main.py 
ARGS:
[1] path to csv file
[2] path to openrefine programm
[3] path to spark_home *optional
```